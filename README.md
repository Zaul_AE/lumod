<div style="text-align: center;">
<img src="img/lumod_logo.png" alt="" width="180"/>
</div>

# **Lumped Models (LuMod) for Hydrology**

**LuMod** is an easy to use set of Lumped Models for hydrological simulation in Python language.

Compared with other source codes that pretend to be fast, **LuMod** was created to facilitate the modeling and the results processing. Moreover, some features of **LuMod** are compiled with [numba](http://numba.pydata.org/) to increase simulation speed.


## **Documentation**

Full documentation and examples are available here: [LuMod-Docs](https://zaul_ae.gitlab.io/lumod-docs)

Installation is available from PyPI: [LuMod project](https://pypi.org/project/lumod/)

If you are not familiar with Python, try our Web Application: [LuMod-App](https://share.streamlit.io/saularciniegaesparza/lumod-app/main/app.py)

## **Key Features**

**LuMod** incorporates well know hydrological models with different structures:

* **MILC**: One layer Lumped version of the MISDc model adapted for continous daily simulation.
* **HYMOD**: Rainfall-Runoff Model based on the Probability-Distributed Model concept that runs at daily timestep.
* **HBV**: Modified version of the Hydrologiska Byråns Vattenbalansavdelning (HBV) model.
* **GR4J**: Well known daily rain-runoff model that depends of four parameters.
* **GR2M**: Monthly rain-runoff model that depends of two parameters.
* **GR1A**: One parameter annual rain-runoff model.
* **MonteCarlo**: Random-based simulation for parameters optimization and uncertainty analysis.

## **Quick installation**

The easier way to install **LuMod** is using [PyPI](https://pypi.org/):

```bash
pip install lumod
```

## **Basic Example**

**LuMod** was thought to be easy to use and compatible with libraries dedicated to DataScience, so it works with Pandas and Matplotlib to facilitate the analysis of simulations.

```python
# Import modules
import lumod
from lumod import tools

# Load example data
info, forcings = lumod.load_example(2)

# Create a model
parameters = {"x1": 500, "x3": 200}  # define some parameters
model = lumod.models.GR4J(area=info.area, lat=info.lat, params=parameters)
print(model)

# Run your model
simulations = model.run(forcings, x2=3.0) # modify parameter x2 before start

# Validate your model
tools.plots.model_evaluation(forcings.prec, forcings.qt, simulations.qt)
```

## **Citation**

*Coming Soon*

## **Author**

**Main Developer**

Saúl Arciniega Esparza, Ph.D., Full Time Associate Professor at the [Faculty of Engineering](https://www.ingenieria.unam.mx/) at the [National Autonomous University of Mexico](https://www.unam.mx/), working on the [Hydrogeology Group](https://www.ingenieria.unam.mx/hydrogeology/).

[LinkedIn](https://www.linkedin.com/in/saularciniegaesparza/) | [Twitter](https://twitter.com/zaul_arciniega) | [ResearchGate](https://www.researchgate.net/profile/Saul-Arciniega-Esparza)


**Collaborators**

Christian Birkel, Ph.D., Full Time Professor and Researcher at the Departament of Geography at [University of Costa Rica](https://www.ucr.ac.cr/), and leader of the [Observatory of Water and Global Change (OACG)](https://www.oacg.fcs.ucr.ac.cr/?fbclid=IwAR2Z2izD2Nrj8n7KnBuH69iGnsoUKirixrN1Y7Rd4uBo6K5zjo4dhFrYgIc).

[Facebook](https://www.facebook.com/OACG.UCR) | [ResearchGate](https://www.researchgate.net/profile/Christian_Birkel)


## **Acknowledgments**

**Funding**

The [National Council of Science and Technology (CONACYT)](https://conacyt.mx/), the [Leverhulme Trust](https://www.leverhulme.ac.uk/) and the [German Academic Exchange Service (DAAD)](https://www.daad.de/en/) are thanked for partial funding of this work.
